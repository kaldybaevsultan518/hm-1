const olTag=document.querySelector('ol')
const all=document.querySelector('#all')
const acer=document.querySelector('#acer')
const mac=document.querySelector('#macbook')
const hp=document.querySelector('#hp')
const lin=document.querySelector('#linova')
const inputs=document.querySelectorAll('input')
const add=document.querySelector('#add')

const vse=[
    {name:'Acer', img:'https://softech.kg/image/cache/f547ecc97f37735b175c4226b2188f95.png', ssd:350, price:43000},
    {name:'Macbook', img:'https://www.stmgoods.com.au/wp-content/uploads/sites/2/STM-Studio-Macbook-Pro-14-16-2021-Clear-rear-open.png', ssd:349, price:90000},
    {name:'HP', img:'https://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c06242267.png', ssd:440, price:58000},
    {name:'Lenovo', img:'https://aztech.az/wp-content/uploads/2022/09/Lenovo-ThinkBook-15-G2-ITL-2.png', ssd:389, price:39000}
]
all.onclick=()=>{
    showLaptop(vse)
}
function showLaptop(vse){
    olTag.innerHTML=''
    for (const all of vse){
        olTag.innerHTML+=`
        <li style=color:white>
            <h4>Name: ${all.name}</h4>
            <img style='width:90%' src=${all.img} />
            <p>Memory: ${all.ssd} Gb </p>
            <p>Price: ${all.price} som </p>
        </li>`
    }
}

acer.onclick=()=>{
    const kol=vse
    .filter(todo=>{
        return todo.name==='Acer'
    })
    setTimeout(()=>{
        showLaptop(kol)
    }, 1000)

    

}
mac.onclick=()=>{
    const lok=vse
    .filter(todo=>{
        return todo.name==='Macbook'
    })
    setTimeout(()=>{
        showLaptop(lok)
    }, 1000)
}
    
hp.onclick=()=>{
    const pol=vse
    .filter(todo=>{
        return todo.name==='HP'
    })
    setTimeout(()=>{
        showLaptop(pol)
    }, 1000)
}

lin.onclick=()=>{
    const jop=vse
    .filter(todo=>{
        return todo.name==='Lenovo'
    })
    setTimeout(()=>{
        showLaptop(jop)
    }, 1000)
}

add.onclick=()=>{
    const newl={
    name:inputs[0].value,
    img:inputs[1].value,
    ssd:inputs[2].value,
    price:inputs[3].value
}
    vse.push(newl)
    showLaptop(add)
} 